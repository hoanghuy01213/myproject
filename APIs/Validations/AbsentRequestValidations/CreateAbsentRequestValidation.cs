﻿using Applications.ViewModels.AbsentRequest;
using FluentValidation;

namespace APIs.Validations.AbsentRequestValidations
{
    public class CreateAbsentRequestValidation : AbstractValidator<CreateAbsentRequestViewModel>
    {
        public CreateAbsentRequestValidation()
        {
            RuleFor(x => x.AbsentDate).GreaterThanOrEqualTo(x => DateTime.Today.Date);
        }
    }
}