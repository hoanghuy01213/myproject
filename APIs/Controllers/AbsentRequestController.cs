﻿using Applications.Interfaces;
using Applications.ViewModels.AbsentRequest;
using Applications.ViewModels.Response;
using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using System.Drawing;

namespace APIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AbsentRequestController : ControllerBase
    {
        private readonly IAbsentRequestServices _absentrequestService;
        private readonly IValidator<CreateAbsentRequestViewModel> _validatorCreate;

        public AbsentRequestController(IAbsentRequestServices absentrequestServices,
            IValidator<CreateAbsentRequestViewModel> validatorCreate
             )
        {
            _absentrequestService = absentrequestServices;
            _validatorCreate = validatorCreate;
        }
        //[HttpGet("GetAllAbsentRequestByEmail/{Email}")]
        //public async Task<Response> GetAllAbsentRequestByEmail(string Email, int pageIndex = 0, int pageSize = 10) => await _absentrequestService.GetAllAbsentRequestByEmail(Email, pageIndex, pageSize);
        
        [HttpGet("GetAbsentById/{AbsentId}")]
        public async Task<Response> GetAbsentById(Guid AbsentId) => await _absentrequestService.GetAbsentById(AbsentId);

        [HttpPost("CreateAbsentRequest")]
        public async Task<Response> CreateAbsentRequest(CreateAbsentRequestViewModel absentRequestViewModel)
        {
            ValidationResult result = _validatorCreate.Validate(absentRequestViewModel);
            if(result.IsValid)
            {
                return await _absentrequestService.CreateAbsentRequest(absentRequestViewModel);
            }

            return new Response(System.Net.HttpStatusCode.BadRequest, "Please Check Date Time Again, Must be Greater than Today");
        }
        
    }
}
