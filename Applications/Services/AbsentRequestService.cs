﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels.AbsentRequest;
using Applications.ViewModels.Response;
using AutoMapper;
using DocumentFormat.OpenXml.Office.CustomUI;
using Domain.Entities;
using System.Net;

namespace Applications.Services
{
    public class AbsentRequestService : IAbsentRequestServices
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public AbsentRequestService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
       
        public async Task<Response> GetAllAbsentRequestByEmail(string Email, int pageIndex = 0, int pageSize = 10)
        {
            var AbsentRequestObj = await _unitOfWork.AbsentRequestRepository.GetAllAbsentRequestByEmail(Email, pageIndex, pageSize);
            if (AbsentRequestObj.Items.Count() < 1) return new Response(HttpStatusCode.BadRequest, "Id not found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", _mapper.Map<Pagination<AbsentRequestViewModel>>(AbsentRequestObj));
        }

        public async Task<Response> GetAbsentById(Guid AbsentId)
        {
            var absObj = await _unitOfWork.AbsentRequestRepository.GetByIdAsync(AbsentId);
            var result = _mapper.Map<AbsentRequestViewModel>(absObj);
            var createBy = await _unitOfWork.UserRepository.GetByIdAsync(absObj?.CreatedBy);
            if (createBy != null)
            {
                result.CreatedBy = createBy.Email;
            }
            if (absObj == null) return new Response(HttpStatusCode.BadRequest, "Id not found");
            else return new Response(HttpStatusCode.OK, "Search succeed", result);
        }

        public async Task<Response?> CreateAbsentRequest(CreateAbsentRequestViewModel absentRequest)
        {
            //var Class = await _unitOfWork.ClassRepository.GetClassByClassCode(absentRequest.ClassCode);
            //var User = await _unitOfWork.UserRepository.GetUserByEmail(absentRequest.Email);
            var ClassUser = await _unitOfWork.AttendanceRepository.GetSingleAttendanceForUpdate(absentRequest.AbsentDate.Date, absentRequest.UserId, absentRequest.ClassId);
            if (ClassUser != null) 
            {
                var AbsentOjb = _mapper.Map<AbsentRequest>(absentRequest);
                await _unitOfWork.AbsentRequestRepository.AddAsync(AbsentOjb);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                if (isSuccess)
                {
                    return new Response(HttpStatusCode.OK, "Create Absent Request Succeed");
                }
            }           
            return new Response(HttpStatusCode.BadRequest, "Create Absent Request Fail, check Class Code or User Email again");
        }
    }
}
