﻿namespace Applications.ViewModels.SyllabusViewModels
{
    public class ModuleCreate
    {
        public string ModuleName { get; set; }
        public List<UnitCreate>? Units { get; set; }
    }
}
