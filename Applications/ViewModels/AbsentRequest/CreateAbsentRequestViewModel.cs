﻿

namespace Applications.ViewModels.AbsentRequest
{
    public class CreateAbsentRequestViewModel
    {
        public string AbsentReason { get; set; }
        public DateTime AbsentDate { get; set; }
        public Guid ClassId { get; set; }
        public Guid UserId { get; set; }
    }
}
