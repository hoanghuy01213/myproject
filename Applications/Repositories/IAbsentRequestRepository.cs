﻿using Applications.Commons;
using Applications.ViewModels.AbsentRequest;
using Domain.Entities;

namespace Applications.Repositories
{
    public interface IAbsentRequestRepository : IGenericRepository<AbsentRequest>
    {
        Task<Pagination<AbsentRequest>> GetAllAbsentRequestByEmail(string Email, int pageNumber = 0, int pageSize = 10);
        Task AddAbsentRequest(string ClassCode, string Email, AbsentRequest AbsentDTO);
        Task<AbsentRequest> GetSingleAbsentRequest(string ClassCode, string Email);
    }
}
