﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.Repositories;
using Applications.ViewModels.AbsentRequest;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructures.Repositories
{
    public class AbsentRequestRepository : GenericRepository<AbsentRequest>, IAbsentRequestRepository
    {
        private readonly AppDBContext _dbContext;
        private readonly IClaimService _claimService;
        public AbsentRequestRepository(AppDBContext dbContext, ICurrentTime currentTime, IClaimService claimService) : base(dbContext, currentTime, claimService)
        {
            _dbContext = dbContext;
            _claimService = claimService;
        }

        public async Task AddAbsentRequest(string ClassCode, string Email, AbsentRequest AbsentDTO)
        {
            foreach(var item in _dbContext.ClassUser.Where(x => x.Class.ClassCode == ClassCode && x.User.Email == Email))
            {
                AbsentDTO.CreationDate = DateTime.UtcNow;
                AbsentDTO.CreatedBy = _claimService.GetCurrentUserId;
                AbsentDTO.IsAccepted = false;
                AbsentDTO.AbsentDate = AbsentDTO.AbsentDate.Date;
                AbsentDTO.UserId = item.UserId;
                AbsentDTO.ClassId = item.ClassId;
                AbsentDTO.IsDeleted = false;
                await _dbContext.AddAsync(AbsentDTO);
            }
        }

        public async Task<Pagination<AbsentRequest>> GetAllAbsentRequestByEmail(string Email, int pageNumber = 0, int pageSize = 10)
        {
            var itemCount = await _dbContext.AbsentRequests.CountAsync();
            var items = await _dbSet.Where(x => x.UserId.Equals(Email))
                                    .OrderByDescending(x => x.CreationDate)
                                    .Skip(pageNumber * pageSize)
                                    .Take(pageSize)
                                    .AsNoTracking()
            .ToListAsync();

            var result = new Pagination<AbsentRequest>()
            {
                PageIndex = pageNumber,
                PageSize = pageSize,
                TotalItemsCount = itemCount,
                Items = items,
            };

            return result;
        }

        public Task<AbsentRequest> GetSingleAbsentRequest(string ClassCode, string Email)
        {
            return _dbSet.FirstOrDefaultAsync(x => x.Class.ClassCode == ClassCode && x.User.Email == Email);
        }
    }
}
